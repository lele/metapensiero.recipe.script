Changes
-------

1.2 (2014-03-08)
~~~~~~~~~~~~~~~~

* Simple test suite

* Fix return of installed stuff in Python scripts


1.1 (2014-03-08)
~~~~~~~~~~~~~~~~

* First release on PyPI


1.0 (2014-03-08)
~~~~~~~~~~~~~~~~

* Initial version, extracted from SoLista
